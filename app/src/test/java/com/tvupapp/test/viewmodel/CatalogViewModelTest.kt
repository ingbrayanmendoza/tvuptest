package com.tvupapp.test.viewmodel

import android.os.Build
import com.tvupapp.test.api.status.Result
import com.tvupapp.test.repository.CatalogRepository
import com.tvupapp.test.testutils.getOrAwaitValue
import com.tvupapp.test.testutils.mock.CatalogResponseMock.getSections
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@ExperimentalCoroutinesApi
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class CatalogViewModelTest {

    private val catalogRepositoryMock = Mockito.mock(CatalogRepository::class.java)

    private lateinit var viewModel: CatalogViewModel

    @Before
    fun setUp() {
        viewModel = CatalogViewModel().apply {
            catalogRepository = catalogRepositoryMock
        }
    }

    @Test
    fun getCatalog_shouldCallRepository_andCatalogResultSuccess() = runBlockingTest {
        val catalogResponseMock = getSections()
        Mockito.`when`(catalogRepositoryMock.getCatalog()).thenReturn(
            Result.success(catalogResponseMock))

        val expected = CatalogViewModel.CatalogUiState.Success(catalogResponseMock)

        viewModel.getCatalog()

        assertEquals(expected, viewModel.uiState.getOrAwaitValue())
    }

    @Test
    fun getCatalog_shouldCallRepository_andCatalogResultError() = runBlockingTest {
        Mockito.`when`(catalogRepositoryMock.getCatalog()).thenReturn(
            Result.serverError())

        val expected = CatalogViewModel.CatalogUiState.ServerError

        viewModel.getCatalog()

        assertEquals(viewModel.uiState.getOrAwaitValue(), expected)
    }
}