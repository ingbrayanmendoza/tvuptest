package com.tvupapp.test.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import com.tvupapp.test.databinding.ShowAdapterBinding
import com.tvupapp.test.model.Show
import com.tvupapp.test.testutils.TestApplication
import com.tvupapp.test.testutils.mock.CatalogResponseMock.getShows
import com.tvupapp.test.ui.adapter.ShowAdapter
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = TestApplication::class)
class ShowAdapterTest {

    private lateinit var adapter: ShowAdapter
    private lateinit var shows: List<Show>
    private val onClickListener = object :
        ShowAdapter.OnClickItemListener {
        override fun onClick(show: Show) {
            // Do nothing
        }
    }

    @Before
    fun setUp() {
        shows = getShows()
        adapter = ShowAdapter(shows, onClickListener)
    }

    @Test
    fun onCreateViewHolder_shouldReturnDetailViewHolder() {
        val view: ViewGroup = LinearLayout(getApplicationContext())
        Assert.assertNotNull(adapter.onCreateViewHolder(view, 0))
    }

    @Test
    fun onBindViewHolder_shouldBindViewHolder() {
        val binding = ShowAdapterBinding.inflate(LayoutInflater.from(getApplicationContext()))
        val viewHolder = Mockito.spy(adapter.ShowViewHolder(binding))

        adapter.onBindViewHolder(viewHolder, 0)

        Mockito.verify(viewHolder).bind(shows[0])
    }

    @Test
    fun getItemCount_shouldReturnDetailsSize() {
        Assert.assertEquals(shows.size, adapter.itemCount)
    }
}