package com.tvupapp.test.testutils.mock

import com.tvupapp.test.model.MoviePictures
import com.tvupapp.test.model.Section
import com.tvupapp.test.model.Show

object CatalogResponseMock {

    fun getSections(): Array<Section> {
        return arrayOf(
            Section(
                1,
                "xxxxxx",
                getShows()
            )
        )
    }

    fun getShows(): List<Show> {
        return listOf(
            Show(
                "eventId",
                "language",
                "synopsisEpisode",
                "title",
                1990,
                5f,
                "ageCode",
                MoviePictures("photo","poster","background"),
                arrayOf("country"),
                arrayOf("actors"),
                arrayOf("directors"),
                arrayOf("writers")
            )
        )
    }
}