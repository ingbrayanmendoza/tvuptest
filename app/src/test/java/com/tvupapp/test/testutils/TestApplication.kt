package com.tvupapp.test.testutils

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

/**
 * Test Application used by Robolectric for unit testing instead of actual TVUpApplication.
 *
 * @see "http://robolectric.org/custom-test-runner/"
 */
class TestApplication : Application() {

    private var initialized = false

    override fun onCreate() {
        if (!initialized) {
            Fresco.initialize(this)
            initialized = true
        }
    }
}
