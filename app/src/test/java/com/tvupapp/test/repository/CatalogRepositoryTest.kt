package com.tvupapp.test.repository

import com.google.gson.Gson
import com.tvupapp.test.api.status.Status
import com.tvupapp.test.testutils.mock.CatalogResponseMock.getSections
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.Response

class CatalogRepositoryTest {

    private companion object {
        val mediaType = "application/json; charset=utf-8".toMediaType()
        val body = Gson().toJson(getSections()).toResponseBody(mediaType)
        var successResponse: Response<ResponseBody> = Response.success(body)
        var errorResponse: Response<ResponseBody> = Response.error<ResponseBody>(400, body)
    }

    private lateinit var repository: CatalogRepository
    private var catalogServiceMock = Mockito.mock(CatalogService::class.java)

    @Before
    fun setUp() {
        repository = Mockito.spy(CatalogRepository()).apply {
            catalogService = catalogServiceMock
        }
    }

    @Test
    fun getCatalog_whenIsSuccessful_shouldReturnResponseSuccess() {
        runBlocking {
            Mockito.`when`(catalogServiceMock.getCatalog()).thenReturn(successResponse)

            val result = repository.getCatalog()
            assertEquals(result.status, Status.SUCCESS)
        }
    }

    @Test
    fun getCatalog_whenIsFail_shouldReturnResponseError() {
        runBlocking {
            Mockito.`when`(catalogServiceMock.getCatalog()).thenReturn(errorResponse)

            val result = repository.getCatalog()
            assertEquals(result.status, Status.SERVER_ERROR)
        }
    }
}