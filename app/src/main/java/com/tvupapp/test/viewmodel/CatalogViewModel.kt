package com.tvupapp.test.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tvupapp.test.api.status.Status.SERVER_ERROR
import com.tvupapp.test.api.status.Status.SUCCESS
import com.tvupapp.test.model.Section
import com.tvupapp.test.repository.CatalogRepository
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CatalogViewModel: ViewModel() {

    var catalogRepository = CatalogRepository()

    private val _uiState = MutableLiveData<CatalogUiState>()
    val uiState: LiveData<CatalogUiState>
        get() = _uiState

    private fun showLoading() {
        _uiState.value = CatalogUiState.Loading
    }

    private val coroutineExceptionHandler = CoroutineExceptionHandler { _, _ ->
        _uiState.value = CatalogUiState.ConnectionError
    }

    fun getCatalog() {
        viewModelScope.launch(Dispatchers.Main + coroutineExceptionHandler) {
            showLoading()
            val response = catalogRepository.getCatalog()
            when (response.status) {
                SUCCESS -> { response.data?.let { data -> _uiState.value =
                    CatalogUiState.Success(data)
                } }
                SERVER_ERROR -> { _uiState.value = CatalogUiState.ServerError
                }
            }
        }
    }

    sealed class CatalogUiState {
        object Loading : CatalogUiState()
        object ServerError : CatalogUiState()
        object ConnectionError : CatalogUiState()
        data class Success(val data: Array<Section>) : CatalogUiState()
    }
}
