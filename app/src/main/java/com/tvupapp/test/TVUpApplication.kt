package com.tvupapp.test

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class TVUpApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        initFresco()
    }

    private fun initFresco() {
        Fresco.initialize(applicationContext)
    }
}