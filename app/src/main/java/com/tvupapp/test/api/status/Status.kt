package com.tvupapp.test.api.status

enum class Status {
    SUCCESS,
    SERVER_ERROR
}
