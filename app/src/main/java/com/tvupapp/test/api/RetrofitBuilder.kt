package com.tvupapp.test.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {

    private const val BASE_URL = "https://mediasync.tvup.cloud/"
    private lateinit var httpClient: Retrofit

    fun getClient() = if (::httpClient.isInitialized) httpClient else initRetrofit()

    private fun initRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}