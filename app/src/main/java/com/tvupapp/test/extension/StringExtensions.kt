package com.tvupapp.test.extension

fun Array<String>.converterToString(separator: String): String {
    val text = StringBuilder("")
    if (!isNullOrEmpty()) {
        for (i in indices) {
            text.append(get(i))
            if(i != size - 1) text.append("$separator ")
        }
    }
    return text.toString()
}

fun String.builderUrlImage() = "https://media.tvup.cloud$this"