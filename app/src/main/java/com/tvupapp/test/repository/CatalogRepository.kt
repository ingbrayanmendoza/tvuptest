package com.tvupapp.test.repository

import com.google.gson.Gson
import com.tvupapp.test.api.RetrofitBuilder
import com.tvupapp.test.api.status.Result
import com.tvupapp.test.model.Section
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CatalogRepository {

    var catalogService = RetrofitBuilder.getClient().create(CatalogService::class.java)

    suspend fun getCatalog(): Result<Array<Section>> =
        withContext(Dispatchers.IO) {
            val response = catalogService.getCatalog()
            return@withContext if (response.isSuccessful) {
                Result.success(
                    Gson().fromJson(
                        response.body()?.charStream(),
                        Array<Section>::class.java
                    )
                )
            } else
                Result.serverError()
        }
}
