package com.tvupapp.test.repository

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET

interface CatalogService {

    @GET("media/carrier/5efb4af4f0e17b0075ced97b/last7d.cine.json")
    suspend fun getCatalog(): Response<ResponseBody>
}