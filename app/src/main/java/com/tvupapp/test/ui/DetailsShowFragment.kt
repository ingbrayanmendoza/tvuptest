package com.tvupapp.test.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.tvupapp.test.R
import com.tvupapp.test.databinding.DetailsShowFragmentBinding
import com.tvupapp.test.extension.builderUrlImage
import com.tvupapp.test.extension.converterToString
import com.tvupapp.test.model.Show
import com.tvupapp.test.ui.CatalogFragment.Companion.SHOW_KEY

class DetailsShowFragment: Fragment() {

    companion object {
        const val NAME_VIEW = "DetailsShowFragment"
    }

    private lateinit var binding: DetailsShowFragmentBinding
    private lateinit var show: Show

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        show = arguments?.getSerializable(SHOW_KEY) as Show
        Firebase.analytics.logEvent(
            FirebaseAnalytics.Event.SCREEN_VIEW,
            Bundle().apply {
                putString(FirebaseAnalytics.Param.SCREEN_NAME, NAME_VIEW)
                putString(FirebaseAnalytics.Param.ITEM_ID, show.eventId)
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DetailsShowFragmentBinding.inflate(inflater, container, false)

        bindScreen(show)
        return binding.root
    }

    private fun bindScreen(show: Show) {
        with(binding) {
            backButton.setOnClickListener{ findNavController().navigateUp() }
            imagePoster.setImageURI(show.moviePictures.background.builderUrlImage())
            title.text = show.title
            synopsis.text = HtmlCompat.fromHtml(show.synopsisEpisode, HtmlCompat.FROM_HTML_MODE_LEGACY)
            rating.rating = show.score
            ageCode.text = show.ageCode
            lenguaje.text = show.language
            directors.text = "${resources.getString(R.string.director)} ${show.directors.converterToString(",")}"
            writers.text = "${resources.getString(R.string.writes)} ${show.writers.converterToString(",")}"
        }
    }
}
