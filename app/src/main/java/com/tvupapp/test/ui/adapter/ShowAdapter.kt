package com.tvupapp.test.ui.adapter

import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.tvupapp.test.databinding.ShowAdapterBinding
import com.tvupapp.test.extension.builderUrlImage
import com.tvupapp.test.model.Show
import com.tvupapp.test.ui.adapter.ShowAdapter.ShowViewHolder

class ShowAdapter(
    private val shows: List<Show>,
    private val onClickItemListener: OnClickItemListener
): Adapter<ShowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowViewHolder {
        return ShowViewHolder(ShowAdapterBinding.inflate(from(parent.context)))
    }

    override fun getItemCount() = shows.size

    override fun onBindViewHolder(holder: ShowViewHolder, position: Int) {
        holder.bind(shows[position])
    }

    inner class ShowViewHolder(private val binding: ShowAdapterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(show: Show) {
            with(binding) {
                showContainer.setOnClickListener { onClickItemListener.onClick(show) }
                show.moviePictures.poster?.let { image.setImageURI(it.builderUrlImage())}
                title.text = show.title
                score.rating = show.score
            }
        }
    }

    interface OnClickItemListener {
        fun onClick(show: Show)
    }
}
