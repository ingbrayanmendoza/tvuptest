package com.tvupapp.test.ui.adapter

import android.view.LayoutInflater.from
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.tvupapp.test.databinding.SectionAdapterBinding
import com.tvupapp.test.model.Section
import com.tvupapp.test.ui.adapter.ShowAdapter.OnClickItemListener

class SectionAdapter(
    private val sections: Array<Section>,
    private val onClickItemListener: OnClickItemListener
) : Adapter<SectionAdapter.SectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        return SectionViewHolder(SectionAdapterBinding.inflate(from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = sections.size

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        holder.bind(sections[position])
    }

    inner class SectionViewHolder(private val binding: SectionAdapterBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(section: Section) {
            binding.shows.adapter = ShowAdapter(section.shows, onClickItemListener)
        }
    }
}
