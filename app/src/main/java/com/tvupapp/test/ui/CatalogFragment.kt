package com.tvupapp.test.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.tvupapp.test.R
import com.tvupapp.test.databinding.CatalogFragmentBinding
import com.tvupapp.test.model.Section
import com.tvupapp.test.model.Show
import com.tvupapp.test.ui.adapter.SectionAdapter
import com.tvupapp.test.ui.adapter.ShowAdapter.OnClickItemListener
import com.tvupapp.test.viewmodel.CatalogViewModel
import com.tvupapp.test.viewmodel.CatalogViewModel.CatalogUiState

class CatalogFragment: Fragment(), OnClickItemListener {

    companion object {
        const val NAME_VIEW = "CatalogFragment"
        const val SHOW_KEY = "show_key"
    }

    private lateinit var binding: CatalogFragmentBinding
    private val viewModel: CatalogViewModel by activityViewModels()

    private val uiStateObserver: Observer<CatalogUiState> =
        Observer { uiStateResponse ->
            when (uiStateResponse) {
                is CatalogUiState.Loading -> showLoading()
                is CatalogUiState.ServerError -> showServerErrorScreen()
                is CatalogUiState.ConnectionError -> showConnectionErrorScreen()
                is CatalogUiState.Success -> bindScreen(uiStateResponse.data)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Firebase.analytics.logEvent(
            FirebaseAnalytics.Event.SCREEN_VIEW,
            Bundle().apply {
                putString(FirebaseAnalytics.Param.SCREEN_NAME, NAME_VIEW)
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CatalogFragmentBinding.inflate(inflater, container, false)

        viewModel.uiState.observe(viewLifecycleOwner, uiStateObserver)
        viewModel.getCatalog()
        return binding.root
    }

    private fun showLoading() {
        binding.loadingAnimation.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.loadingAnimation.visibility = View.INVISIBLE
    }

    private fun showServerErrorScreen() {
        hideLoading()
        findNavController().navigate(R.id.action_catalogFragment_to_feedbackServerErrorFragment)

    }

    private fun showConnectionErrorScreen() {
        hideLoading()
        findNavController().navigate(R.id.action_catalogFragment_to_feedbackServerErrorFragment)
    }

    private fun bindScreen(data: Array<Section>) {
        hideLoading()
        with(binding) {
            sections.visibility = View.VISIBLE
            sections.adapter = SectionAdapter(data, this@CatalogFragment)
        }
    }

    override fun onClick(show: Show) {
        val bundle = Bundle().apply { putSerializable(SHOW_KEY, show) }
        findNavController().navigate(R.id.action_catalogFragment_to_detailsShowFragment, bundle)
    }
}
