package com.tvupapp.test.model

import java.io.Serializable

data class Show(
        val eventId: String,
        val language: String,
        val synopsisEpisode: String,
        val title: String,
        val year: Int,
        val score: Float,
        val ageCode: String,
        val moviePictures: MoviePictures,
        val country: Array<String>,
        val actors: Array<String>,
        val directors: Array<String>,
        val writers: Array<String>
) : Serializable
