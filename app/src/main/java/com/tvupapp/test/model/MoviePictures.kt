package com.tvupapp.test.model

import java.io.Serializable

data class MoviePictures(val photo: String, val poster: String?, val background: String) : Serializable
