package com.tvupapp.test.model

import java.io.Serializable

data class Section(val totalCount: Int, val genre: String, val shows: List<Show>): Serializable
